console.log("Running example ...!!");

const PROP_NAME = "name";

const obj = {
  [PROP_NAME]: "John",
  age: 30,
  isMarried: false,
  address: {
    street: "50 Main st",
    city: "Boston",
    state: "MA",
  },
};

const addOn = {
  gugus: "daada",
  age: 77,
};

const copy = {
  ...obj,
  age: 42,
  ...addOn,
  address: { ...obj.address, city: "Bern" },
};

console.log("equal?", obj === copy);
console.log("equal address?", obj.address === copy.address);
console.log("copy", copy);

//
// async function main() {
//   try {
//     let lukeResponse = await axios.get("https://swapi.dev/api/people/1/");
//     console.log(lukeResponse.data);
//     let filmRespones = await axios.get(lukeResponse.data.films[0] + "/XXX");
//     console.log(filmRespones.data);
//     let planetResponse = await axios.get(filmRespones.data.planets[0]);
//     console.log(planetResponse.data);
//   } catch (error) {
//     console.log("ERRRORR", error);
//   }
// }
// main();
// console.log("Done!");

// function main() {
//   axios
//     .get("https://swapi.dev/api/people/1/")
//     .then((r) => {
//       console.log("Luke", r.data);
//       return axios.get(r.data.films[0]);
//     })
//     .then((r) => {
//       console.log("First film", r.data);
//       return axios.get(r.data.planets[0]);
//     })
//     .then((r) => {
//       console.log("First planet", r.data);
//     })
//   console.log("Done");
// }
// main();

//
// $.get("https://swapi.dev/api/people/1/", (r) => {
//   console.log("Luke", r);
//   $.get(r.films[0], (r) => {
//     console.log("First film", r);
//     $.get(r.planets[0], (r) => {
//       console.log("First planet", r);
//     });
//     console.log("2");
//   });
//   console.log("1");
// });
//
// console.log("Go to other module");
