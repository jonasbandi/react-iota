import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Person } from './star-wars/Person';
import { Home } from './star-wars/Home';
import { PersonScreen } from './star-wars/person/PersonScreen';
import { HomeScreen } from './star-wars/home/HomeScreen';

function App() {

    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                {/*<Route path="persob" component={<Person/>}></Route>*/}
                {/*<Route path="hone" component={<Home/>}></Route>*/}
                <PersonScreen/>
                <HomeScreen/>
            </header>
        </div>
    );
}

export default App;


