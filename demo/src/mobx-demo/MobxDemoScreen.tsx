import { appState } from "./model";
import { observer } from "mobx-react-lite";

function MobxDemoScreenComponent() {
  return (
    <div style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <h1>Mobx Demo Screen</h1>
      <div>
        <button onClick={() => appState.loadHero()}>Load ...</button>
      </div>
      <h1>Hero Name: {appState.hero?.name}</h1>
    </div>
  );
}

export const MobxDemoScreen = observer(MobxDemoScreenComponent);
