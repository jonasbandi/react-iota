import { useStarWarsPerson } from './api/useStarWarsPerson';

export function Home() {

    let {person} = useStarWarsPerson(2);

    return (
        <div>
            Display of Home. {person?.name}
        </div>
    );
}
