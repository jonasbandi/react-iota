import useAxios from 'axios-hooks';
import { StarWarsPerson } from './model';

export function useStarWarsPerson(id: number){

    // const [person, setPerson, gugus] = useState<any>();
    // const [isLoading, setIsLoading] = useState(true);
    // const [error, setError] = useState<any>();
    //
    // useEffect( () => {
    //     async function getData(){
    //         console.log('Mounting Person Component');
    //         try {
    //             setIsLoading(true);
    //             console.log('Axios returned');
    //             const response = await axios.get(`https://swapi.dev/api/people/${id}/`);
    //             setPerson(response.data);
    //             setIsLoading(false);
    //         } catch (e: any){
    //             setError(e);
    //         }
    //     }
    //     getData();
    // }, [])

    const [{data: person, loading: isLoading, error}, refetch] =
        useAxios<StarWarsPerson>(`https://swapi.dev/api/people/${id}/`)

    return {
        person,
        isLoading,
        error,
        refetch
    };
}
