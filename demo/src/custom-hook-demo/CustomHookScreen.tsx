import { useHeroData } from "./useHeroData";

export function CustomHookScreen() {
  const { heroData, loading } = useHeroData();

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>Custom Hook</h1>
      <h1>Hero Name: {heroData.name}</h1>
    </div>
  );
}
