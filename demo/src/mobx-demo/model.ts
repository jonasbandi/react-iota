import { makeAutoObservable, makeObservable } from "mobx";

interface Hero {
  name: string;
}

class AppState {
  hero: Hero | null = null;

  constructor() {
    makeAutoObservable(this);
  }

  loadHero() {
    setTimeout(() => {
      this.hero = {
        name: "Superman",
      };
      console.log("Hero loaded");
    }, 2000);
  }
}

export const appState = new AppState();
