import React, { useEffect, useRef, useState, memo } from "react";
import logo from "./logo.svg";
import "./App.css";
import axios from "axios";

function App() {
  return (
    <div>
      <h1>Scout App</h1>
      <Participant />
    </div>
  );
}

interface IParticipant {
  name: string;
  height: string;
  mass: string;
}

function Participant() {
  const [activeTabIndex, setActiveTabIndex] = useState(0);
  const [participant, setParticipant] = useState<IParticipant | undefined>(
    undefined
  );

  useEffect(() => {
    axios.get("https://swapi.dev/api/people/1/").then((res) => {
      setParticipant(res.data);
    });
  }, []);

  function goToNextTab() {
    setActiveTabIndex((activeTabIndex + 1) % 3);
  }

  function sendToServer() {
    console.log("Sending to server: ", participant);
  }

  // function updateParticipant(name: string, value: string) {
  function updateParticipant(event: React.ChangeEvent) {
    const target = event.target as HTMLInputElement;
    const value = target.value;
    const name = target.name;
    const newParticipant = { ...participant!, [name]: value };
    setParticipant(newParticipant);
  }

  let tabContent = <div>Loading ...</div>;
  if (participant) {
    let activeTab;
    if (activeTabIndex === 0) {
      activeTab = (
        <PersonalInfo
          participant={participant}
          updateParticipant={updateParticipant}
        />
      );
    } else if (activeTabIndex === 1) {
      activeTab = (
        <BankingInfo
          participant={participant}
          updateParticipant={updateParticipant}
        />
      );
    } else {
      activeTab = <ParentInfo />;
    }
    tabContent = (
      <>
        <button onClick={goToNextTab}>Next Tab</button>
        <div>{activeTab}</div>
      </>
    );
  }

  return (
    <div>
      <h2>Participant</h2>
      <pre>{JSON.stringify(participant)}</pre>
      <button onClick={sendToServer}>Send to Server</button>
      <hr />
      {tabContent}
    </div>
  );
}
interface ParticipantTabProps {
  participant: IParticipant;
  // updateParticipant: (name: string, value: string) => void;
  updateParticipant: (event: React.ChangeEvent) => void;
}
function PersonalInfo({ participant, updateParticipant }: ParticipantTabProps) {
  function updateParticipantField(event: React.ChangeEvent) {
    // const target = event.target as HTMLInputElement;
    // const value = target.value;
    // const name = target.name;
    // updateParticipant(name, value);
    updateParticipant(event);
  }

  return (
    <div>
      <h4>PersonalInfo</h4>
      <input
        name="name"
        value={participant.name}
        onChange={updateParticipantField}
      ></input>
      <input
        name="height"
        value={participant.height}
        onChange={updateParticipantField}
      ></input>
    </div>
  );
}
function BankingInfo({ participant, updateParticipant }: ParticipantTabProps) {
  return (
    <div>
      <h4>BankingInfo</h4>;
      <input
        name="mass"
        value={participant.mass}
        onChange={updateParticipant}
      ></input>
    </div>
  );
}
function ParentInfo() {
  return <div>ParentInfo</div>;
}

export default App;
