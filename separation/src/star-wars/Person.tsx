import { StarWarsPerson } from './api/model';

interface PersonProps {
    person: StarWarsPerson,
    reloadPerson: () => void
}
export function Person({person, reloadPerson}: PersonProps) {
    return (
        <div>
            Display of Person:
            {person.name}
            <div>
                <button onClick={() => reloadPerson()}>refetch</button>
            </div>
        </div>
    );
}
