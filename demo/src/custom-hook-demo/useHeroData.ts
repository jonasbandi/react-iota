import { useEffect, useState } from "react";

interface HeroData {
  name: string;
}

export function useHeroData() {
  const [heroData, setHeroData] = useState<HeroData>({
    name: "",
  });
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setHeroData({ name: "Batman" });
      setLoading(false);
    }, 1000);
  }, []);

  return { heroData, loading };
}
