import { Person } from '../Person';
import { Spinner } from '../../common/Spinner';
import { useStarWarsPerson } from '../api/useStarWarsPerson';

export function PersonScreen() {

    const {person, isLoading, error, refetch} = useStarWarsPerson(1);

    if (isLoading) {
        return <Spinner/>
    }
    if (error) {
        return <div>UPS, it went wrong!</div>
    }
    if (!person){
        throw new Error()
    }

    return <Person person={person} reloadPerson={refetch}/>;
}
